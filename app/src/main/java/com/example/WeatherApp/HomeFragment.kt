package com.example.WeatherApp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment(myContext : HomeActivity, str : String): Fragment() {
    var activity = myContext
    var chosen = str
    var models = ForecastWeatherModel()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        if (chosen.isNullOrEmpty()){
            chosen = "tbilisi"
            Log.d("myTag", "nope")
        }
        getForecastModel(chosen)

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        cityForecastText.text = "${chosen.capitalize()} 5 day forecast"
        toggleButton.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                init(false)
            }
            else {
                init(true)
            }
        }
    }

    private fun getForecastModel(value: String?) {
        if (!value.isNullOrEmpty()) {
            DataLoader.getRequestForQuery("forecast", value, "a77840cefd5a443793bd5e6b54776905", object: CustomCallback{
                override fun onSuccess(result: String) {
                    if (result != "null") {
                        Log.d("mytag", result)
                        models = Gson().fromJson(result, ForecastWeatherModel::class.java)
                        init(true)
                    }
                    else {
                        cityForecastText.text = "City Invalid... Change in profile"
                    }
                }
            })
        }
    }

    private fun init(c : Boolean) {
        recyclerView.layoutManager = LinearLayoutManager(activity)
        val adapter = ForecastRecyclerViewAdapter(models.list, activity, c)
        recyclerView.adapter = adapter
    }

}