package com.example.WeatherApp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {
    private var activity = this
    private var myJson: String? = null
    private var myFavouritesJson: String? = null
    private var auth = FirebaseAuth.getInstance()
    private val user = auth.currentUser
    private val mail = user?.email
    private var favouriteCity = ""
    private var favourites = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        getFavouriteCity()
        loadFragment(HomeFragment(this, favouriteCity), "HomeFragment")

        bottomNavigationView.setOnNavigationItemSelectedListener (navListener)
    }

    private var navListener : BottomNavigationView.OnNavigationItemSelectedListener =  BottomNavigationView.OnNavigationItemSelectedListener {
        when (it.itemId) {
            R.id.dock_home -> { Log.d("myTag", "HOME")
                loadFragment(HomeFragment(activity, favouriteCity), "HomeFragment")
                return@OnNavigationItemSelectedListener true
            }
            R.id.dock_favourites -> {Log.d("myTag", "favourite")
                getFavourites()
                loadFragment(FavouritesFragment(activity, favourites), "FavouritesFragment")
                return@OnNavigationItemSelectedListener true
            }
            R.id.dock_profile -> {Log.d("myTag", "profile")
                getFavourites()
                loadFragment(ProfileFragment(activity, favourites), "ProfileFragment")
                return@OnNavigationItemSelectedListener true
            }
            else -> {
                Log.d("myTag", "BAD")
                return@OnNavigationItemSelectedListener false
            }
        }
    }

    private fun loadFragment(fragment: Fragment, tag: String) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.home_screen, fragment, tag)
        transaction.commit()
    }

    fun signOut() {
        FirebaseAuth.getInstance().signOut()
        startActivity(Intent(this, MainActivity :: class.java))
    }

    private fun getFavouriteCity() {
        var sharedPreferences = getSharedPreferences(mail.toString() + "myCity", Context.MODE_PRIVATE)
        myJson = sharedPreferences.getString("favourite city", null)
        var type = object : TypeToken<String>(){}.type
        if (myJson != null)
            favouriteCity = Gson().fromJson(myJson, type)
    }

    fun setFavouriteCity(city: String) {
        var sharedPreferences = getSharedPreferences(mail.toString() + "myCity", Context.MODE_PRIVATE)
        var editor = sharedPreferences.edit()
        var myJson = Gson().toJson(city)
        editor.putString("favourite city", myJson)
        editor.apply()

        getFavouriteCity()
    }

    private fun getFavourites() : ArrayList<String> {
        var sharedPreferences = getSharedPreferences(mail.toString() + "favourites", Context.MODE_PRIVATE)
        myFavouritesJson = sharedPreferences.getString("favourites", null)
        var type = object : TypeToken<ArrayList<String>>(){}.type
        if (myFavouritesJson != null)
            favourites = Gson().fromJson(myFavouritesJson, type)

        return favourites
    }

    fun setFavourites(city: String) {
        favourites.add(city)
        var sharedPreferences = getSharedPreferences(mail.toString() + "favourites", Context.MODE_PRIVATE)
        var editor = sharedPreferences.edit()
        var myJson = Gson().toJson(favourites)
        editor.putString("favourites", myJson)
        editor.apply()
        favourites.clear()

        getFavourites()
    }
}