package com.example.WeatherApp

interface CustomCallback {
    fun onSuccess(result: String){}
    fun onFailure(errorMassage: String){}
}