package com.example.WeatherApp

import android.annotation.SuppressLint
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.item_posts_recyclerview_layout.view.*
import java.text.SimpleDateFormat

class ForecastRecyclerViewAdapter(private val posts:MutableList<ForecastWeatherModel.ListType>, private val activity: HomeActivity, private val c: Boolean):
    RecyclerView.Adapter<ForecastRecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_posts_recyclerview_layout, parent, false))
    }

    override fun getItemCount() = posts.size

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private lateinit var model:ForecastWeatherModel.ListType
        @SuppressLint("SimpleDateFormat")
        @RequiresApi(Build.VERSION_CODES.O)
        fun onBind() {
            model = posts[adapterPosition]

            Glide.with(activity).load("http://openweathermap.org/img/wn/${model.weather.firstOrNull()?.icon}@2x.png").transition(DrawableTransitionOptions.withCrossFade()).into(itemView.weather_image_view)
            var minTmp = 0.0
            var maxTmp = 0.0
            if (c) {
                minTmp = model.main.temp_min - 273.15
                maxTmp = model.main.temp_max - 273.15
            }
            else {
                minTmp = (model.main.temp_min - 273.15) * 9 / 5 + 32
                maxTmp = (model.main.temp_max - 273.15) * 9 / 5 + 32
            }

            itemView.max_temp_text_view.text = "%.2f".format(maxTmp).toDouble().toString()
            itemView.min_temp_text_view.text = "%.2f".format(minTmp).toDouble().toString()

            val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val outPutFormatDay = SimpleDateFormat("EEEE")
            val outPutFormatMonth = SimpleDateFormat("MMM dd")
            val outPutFormatHH = SimpleDateFormat("HH:mm:ss")

            val date = inputFormat.parse(model.dt_txt)
            if (date != null) {
                itemView.day_name_text_view.text = outPutFormatDay.format(date)
                itemView.date_text_view.text = outPutFormatMonth.format(date)
                itemView.forecast_time.text = outPutFormatHH.format(date)
            }
        }
    }
}