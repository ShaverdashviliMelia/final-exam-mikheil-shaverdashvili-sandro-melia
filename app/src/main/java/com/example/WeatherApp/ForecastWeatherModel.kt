package com.example.WeatherApp

import com.google.gson.annotations.SerializedName

class ForecastWeatherModel {

    val cod: Int = 0
    val message: Int = 0
    val cnt: Int = 0
    var list: MutableList<ListType> = ArrayList<ListType>()
    val city: City = City()


    data class Wind(

        val speed: Double,
        val deg: Int
    )

    data class Weather (

        val id : Int,
        val main : String,
        val description : String,
        val icon : String
    )

    data class Sys (

        val pod : String
    )

    data class Rain (
        @SerializedName("3h")
        val h: Double
    )

    data class Main (

        val temp : Double,
        val feels_like : Double,
        val temp_min : Double,
        val temp_max : Double,
        val pressure : Int,
        val sea_level : Int,
        val grnd_level : Int,
        val humidity : Int,
        val temp_kf : Double
    )

    data class ListType (

        val dt : Int,
        val main : Main,
        val weather : List<Weather>,
        val clouds : Clouds,
        val wind : Wind,
        val rain : Rain,
        val sys : Sys,
        val dt_txt : String
    )

    class Coord {
        val lat : Double = 0.0
        val lon : Double = 0.0
    }

    class Clouds {
        val all : Int = 0
    }

    class City {
        val id: Int = 0
        val name: String = ""
        val coord: Coord = Coord()
        val country: String = ""
        val population: Int = 0
        val timezone: Int = 0
        val sunrise: Int = 0
        val sunset: Int = 0
    }
}



