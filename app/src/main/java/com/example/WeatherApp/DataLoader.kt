package com.example.WeatherApp


import android.util.Log.d
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*


//
//
// // api.openweathermap.org/data/2.5/weather?q={city name}&appid={your api key}


object DataLoader {
    private var retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("http://api.openweathermap.org/data/2.5/")
        .build()

    private var service = retrofit.create(ApiRetrofit::class.java)

    fun getRequestForQuery(path: String, q: String, appid : String, customCallback: CustomCallback) {
        val call = service.getRequestForQuery(path, q, appid)
        call.enqueue(callback(customCallback))
    }

    fun postRequest(path:String, parameters: MutableMap<String, String>,customCallback: CustomCallback) {
        val call = service.postRequest(path, parameters)
        call.enqueue(callback(customCallback))
    }

    private fun callback(customCallback: CustomCallback) = object: Callback<String> {
        override fun onFailure(call: Call<String>, t: Throwable) {
            d("getRequest", "${t.message}")
            customCallback.onFailure(t.message.toString())
        }

        override fun onResponse(call: Call<String>, response: Response<String>) {
            d("getRequest", "${response.body()}")
            customCallback.onSuccess(response.body().toString())
        }

    }
}

interface ApiRetrofit {
    @GET("{path}")
    fun getRequestForQuery(
        @Path("path") path: String?,
        @Query("q") q : String,
        @Query("appid") appid: String): Call<String>



    @FormUrlEncoded
    @POST("{path}")
    fun postRequest(@Path("path") path: String?, parameters:Map<String, String>): Call<String>
}