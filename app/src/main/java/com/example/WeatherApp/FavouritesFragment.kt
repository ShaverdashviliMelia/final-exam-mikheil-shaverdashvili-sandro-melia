package com.example.WeatherApp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_favourites.*
import kotlinx.android.synthetic.main.fragment_home.*

class FavouritesFragment(myContext : HomeActivity, fvts: ArrayList<String>): Fragment() {
    var activity = myContext
    var favourites = fvts
    var model : MutableList<CurrentWeatherModel> = mutableListOf()
    var adapter = CurrentRecyclerViewAdapter(model, activity)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_favourites, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        init()
    }

    private fun init() {


        favourites.forEach {
            getForecastModel(it)}

        favouritesRecyclerView.layoutManager = LinearLayoutManager(activity)
        favouritesRecyclerView.adapter = adapter

    }

    private fun getForecastModel(value: String?) {
        if (!value.isNullOrEmpty()) {
            DataLoader.getRequestForQuery("weather", value, "a77840cefd5a443793bd5e6b54776905", object: CustomCallback{
                override fun onSuccess(result: String) {
                    if (result != "null") {
                        var converted = Gson().fromJson(result, CurrentWeatherModel::class.java)
                        model.add(converted)
                        adapter.notifyDataSetChanged()
                    }
                }
            })
        }
    }
}