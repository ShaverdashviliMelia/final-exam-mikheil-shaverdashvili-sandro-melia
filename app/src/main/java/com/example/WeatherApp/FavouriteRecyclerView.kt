package com.example.WeatherApp

import android.annotation.SuppressLint
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.favourites_item_layout.view.*
import kotlinx.android.synthetic.main.item_posts_recyclerview_layout.view.*
import java.text.SimpleDateFormat

class FavouriteRecyclerView(private val posts:MutableList<String>, private val activity: FavouritesActivity):
    RecyclerView.Adapter<FavouriteRecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.favourites_item_layout, parent, false))
    }

    override fun getItemCount() = posts.size

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private lateinit var model:String
        fun onBind() {
            model = posts[adapterPosition]

            itemView.favourite_city_name.text = model

            itemView.button_remove.setOnClickListener {
                activity.editFavourites(model)

                Toast.makeText(activity, "City Removed", Toast.LENGTH_SHORT).show()
                activity.init()
            }
        }
    }
}