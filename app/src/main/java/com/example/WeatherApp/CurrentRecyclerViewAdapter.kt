package com.example.WeatherApp

import android.annotation.SuppressLint
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.item_current_weather_layout.view.*
import kotlinx.android.synthetic.main.item_posts_recyclerview_layout.view.*
import kotlinx.android.synthetic.main.item_posts_recyclerview_layout.view.date_text_view
import kotlinx.android.synthetic.main.item_posts_recyclerview_layout.view.day_name_text_view
import kotlinx.android.synthetic.main.item_posts_recyclerview_layout.view.max_temp_text_view
import kotlinx.android.synthetic.main.item_posts_recyclerview_layout.view.min_temp_text_view
import kotlinx.android.synthetic.main.item_posts_recyclerview_layout.view.weather_image_view
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class CurrentRecyclerViewAdapter(private val posts:MutableList<CurrentWeatherModel>, private val activity: HomeActivity):
    RecyclerView.Adapter<CurrentRecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_current_weather_layout, parent, false))
    }

    override fun getItemCount() = posts.size

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private lateinit var model:CurrentWeatherModel
        @SuppressLint("SetTextI18n")
        @RequiresApi(Build.VERSION_CODES.O)
        fun onBind() {
            model = posts[adapterPosition]

            Glide.with(activity)
                .load("http://openweathermap.org/img/wn/${model.weather.firstOrNull()?.icon}@2x.png")
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(itemView.weather_image_view)

            var minTmpC = model.main.temp_min - 273.15
            var maxTmpC = model.main.temp_max - 273.15
            var minTmpF = (model.main.temp_min - 273.15) * 9 / 5 + 32
            var maxTmpF = (model.main.temp_max - 273.15) * 9 / 5 + 32

            itemView.max_temp_text_view.text = "${"%.2f".format((minTmpC+maxTmpC)/2).toDouble()} C"
            itemView.min_temp_text_view.text = "${"%.2f".format((minTmpF+maxTmpF)/2).toDouble()} F"

            val current = LocalDateTime.now()
            val getDay = DateTimeFormatter.ofPattern("EEEE")
            val getMonth = DateTimeFormatter.ofPattern("MMM dd")
            val formattedDay = current.format(getDay)
            val formattedMonth = current.format(getMonth)

            itemView.day_name_text_view.text = formattedDay
            itemView.date_text_view.text = formattedMonth
            itemView.city_name.text = model.name
        }
    }
}