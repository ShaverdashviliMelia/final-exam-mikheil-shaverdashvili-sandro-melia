package com.example.WeatherApp

import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment(cntx : HomeActivity, fvts: ArrayList<String>): Fragment() {
    private var activity = cntx
    private var auth = FirebaseAuth.getInstance()
    private val user = auth.currentUser
    private var name :String? = ""
    private var email :String? = ""
    private var uid :String? = ""
    private var photoUrl : Uri? = Uri.EMPTY
    private var favourites = fvts

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        open_profile.setOnClickListener {
            startActivity(Intent(activity, ProfileActivity::class.java))
        }

        edit_favourite_city.setOnClickListener {
            changeCity()
            sign_out_button.setBackgroundColor(Color.TRANSPARENT)
        }

        edit_favourites.setOnClickListener {
            var intent = Intent(activity, FavouritesActivity::class.java)
            startActivity(intent)
        }

        sign_out_button.setOnClickListener {
            activity.signOut()
        }


        init()
    }

    private fun init() {
        getCurrentUserInfo()

        Glide.with(this).load("https://cdn2.iconfinder.com/data/icons/weather-flat-14/64/weather03-512.png").into(profileImageView)
    }

    private fun getCurrentUserInfo() {
        if (user != null) {
            name = user.displayName
            email = user.email
            uid = user.uid
            photoUrl =user.photoUrl
        }

        Log.d("my", "$name $email $uid $photoUrl")
    }

    private fun changeCity() {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Change City")
        val view = layoutInflater.inflate(R.layout.change_city, null)
        val resetMail = view.findViewById<EditText>(R.id.reset_city)

        builder.setView(view)
        builder.setPositiveButton("Reset", DialogInterface.OnClickListener { _, _ ->
            activity.setFavouriteCity(resetMail.text.toString())
        })
        builder.setNegativeButton("Close", DialogInterface.OnClickListener { _, _ ->})
        builder.show()
    }
}