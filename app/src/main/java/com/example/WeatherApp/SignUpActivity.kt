package com.example.WeatherApp


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        init()
    }
    private  fun init() {
        auth = FirebaseAuth.getInstance()

        create_account.setOnClickListener {
            val activity = MainActivity()
            if (activity.validateMail(username) &&
                activity.validatePass(password) &&
                validateSecondPass(password, passwordVerify) &&
                activity.validateFavourite(favourite_city)) {
                createUser(username.text.toString(), password.text.toString())
            }
        }
        back_sign_up.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    private fun validateSecondPass(passIDo : EditText, passIDt : EditText) : Boolean{
        if (passIDo.text.toString() != passIDt.text.toString()) {
            passwordVerify.error = "Please Enter Same Password"
            passwordVerify.requestFocus()

            return false
        }

        return true
    }

    private fun createUser(mail : String, pass : String) {
        auth.createUserWithEmailAndPassword(mail, pass)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    startActivity(Intent(this, MainActivity :: class.java))
                    setFavouriteCity(favourite_city.text.toString())
                    setFavourites(favourite_city.text.toString())
                    finish()
                } else {
                    Toast.makeText(baseContext, "Sign Up Failed", Toast.LENGTH_SHORT).show()

                }
            }

    }

    private fun setFavouriteCity(city: String) {
        var sharedPreferences = getSharedPreferences(username.text.toString() + "myCity", Context.MODE_PRIVATE)
        var editor = sharedPreferences.edit()
        var myJson = Gson().toJson(city)
        editor.putString("favourite city", myJson)
        editor.apply()
    }

    fun setFavourites(city: String) {
        var favourites = ArrayList<String>()
        favourites.add(city)
        var sharedPreferences = getSharedPreferences(username.text.toString() + "favourites", Context.MODE_PRIVATE)
        var editor = sharedPreferences.edit()
        var myJson = Gson().toJson(favourites)
        editor.putString("favourites", myJson)
        editor.apply()
        favourites.clear()
    }

}