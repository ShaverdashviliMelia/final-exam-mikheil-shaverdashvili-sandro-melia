package com.example.WeatherApp

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity() {
    private var auth = FirebaseAuth.getInstance()
    private val user = auth.currentUser
    private var name :String? = ""
    private var email :String? = ""
    private var uid :String? = ""
    private var photoUrl : Uri? = Uri.EMPTY


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        init()
    }


    private fun init() {
        getCurrentUserInfo()

        Glide.with(this).load("https://cdn2.iconfinder.com/data/icons/weather-flat-14/64/weather03-512.png").into(profileImageView)

        profileEmailTextView.text = email

        profileBackButton.setOnClickListener {
            startActivity(Intent(this, HomeActivity :: class.java))
        }
    }

    private fun getCurrentUserInfo() {
        if (user != null) {
            name = user.displayName
            email = user.email
            uid = user.uid
            photoUrl =user.photoUrl
        }

        Log.d("my", "$name $email $uid $photoUrl")
    }


}
