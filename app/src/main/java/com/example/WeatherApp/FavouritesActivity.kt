package com.example.WeatherApp

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_favourites.*

class FavouritesActivity : AppCompatActivity() {
    var favourites = ArrayList<String>()
    private var auth = FirebaseAuth.getInstance()
    private val user = auth.currentUser
    private val mail = user?.email

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourites)

        getFavourites()
        init()
    }

    fun init() {
        recyclerViewFavourites.layoutManager = LinearLayoutManager(this)
        val adapter = FavouriteRecyclerView(favourites, this)
        recyclerViewFavourites.adapter = adapter

        add_favourite_city.setOnClickListener {
            addCity()
        }

        favourites_back_button.setOnClickListener {
            startActivity(Intent(this, HomeActivity :: class.java))
        }
    }

    fun editFavourites(model: String) {
        favourites.removeAll { it == model }
        setFavourites()
    }

    private fun addCity() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Add City")
        val view = layoutInflater.inflate(R.layout.change_city, null)
        val resetMail = view.findViewById<EditText>(R.id.reset_city)

        builder.setView(view)
        builder.setPositiveButton("Add", DialogInterface.OnClickListener { _, _ ->
            favourites.add(resetMail.text.toString())
            setFavourites()
            init()
        })
        builder.setNegativeButton("Close", DialogInterface.OnClickListener { _, _ ->})
        builder.show()
    }

    private fun setFavourites() {
        var sharedPreferences = getSharedPreferences(mail.toString() + "favourites", Context.MODE_PRIVATE)
        var editor = sharedPreferences.edit()
        var myJson = Gson().toJson(favourites)
        editor.putString("favourites", myJson)
        editor.apply()
    }

    fun getFavourites() {
        var sharedPreferences = getSharedPreferences(mail.toString() + "favourites", Context.MODE_PRIVATE)
        var myFavouritesJson = sharedPreferences.getString("favourites", null)
        var type = object : TypeToken<ArrayList<String>>(){}.type
        if (myFavouritesJson != null)
            favourites = Gson().fromJson(myFavouritesJson, type)
    }
}
